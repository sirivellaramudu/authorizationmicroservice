package com.cognizant.groupd.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.groupd.client.CustomerClient;
import com.cognizant.groupd.entity.AccountFinal;
import com.cognizant.groupd.exception.AccountFinalNotFoundException;
import com.cognizant.groupd.exception.TokenExpireException;

import com.cognizant.groupd.model.TransactionRequest;
import com.cognizant.groupd.repository.AccountFinalRepository;

import jakarta.transaction.Transactional;

@Service
public class AccountFinalService {

	@Autowired
	CustomerClient cust;

	@Autowired
	AccountFinalRepository accountFinalRepository;

	@Transactional
	public AccountFinal registerAccount(AccountFinal account, String requestTokenHeader) throws TokenExpireException {
		Boolean result = cust.authorizeTheRequest(requestTokenHeader);
		account.setDateOfOpening(new Date());
		// Account account = accountRepository,save(account);
		
		if (result == true) {
			return accountFinalRepository.save(account);
		} else {
			throw new TokenExpireException("Token is Invalid");
		}
	}
	@Transactional
	public List<AccountFinal> getAllAccount(int custId, String requestTokenHeader) throws TokenExpireException {
		Boolean result = cust.authorizeTheRequest(requestTokenHeader);
		if (result == true) {
			return accountFinalRepository.getAllAccount(custId);
		} else {
			throw new TokenExpireException("Token is Invalid");
		}

	}
	@Transactional
	public AccountFinal getAccount(int accNumber,String requestTokenHeader)
			throws AccountFinalNotFoundException, TokenExpireException{
		Boolean result = cust.authorizeTheRequest(requestTokenHeader);
		if (result == true) {
			AccountFinal account = accountFinalRepository.findById(accNumber).orElse(null);
			if (account != null) {
				return account;
			} else {
				throw new AccountFinalNotFoundException("Account Not Found");
			}
		} else {
			throw new TokenExpireException("Token is Invalid");

		}
	}
	@Transactional
	public AccountFinal updateAccount(TransactionRequest requestType,String requestTokenHeader) throws AccountFinalNotFoundException, TokenExpireException {
		Boolean result = cust.authorizeTheRequest(requestTokenHeader);
		if(result==true) {
		AccountFinal account = accountFinalRepository.findById(requestType.getAccountNumber()).orElse(null);
		if(account!=null) {
			if(requestType.getTransactionType().equalsIgnoreCase("Credit")) {
				account.setBalance(account.getBalance() + requestType.getAmount());
			} else {
				account.setBalance(account.getBalance() - requestType.getAmount());
			}
			return accountFinalRepository.save(account);
		} 
		else {
			throw new AccountFinalNotFoundException("Account number is not valid ..! Please check!");
		}
	} else {
		throw new TokenExpireException(requestTokenHeader);
	}
	}
}
