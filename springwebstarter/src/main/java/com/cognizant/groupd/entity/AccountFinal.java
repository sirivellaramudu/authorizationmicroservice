package com.cognizant.groupd.entity;



import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name="accountFinal")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountFinal {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private int accNo;
	private int custId;
	private String name;
	private String accType;
	private double balance;
	@Temporal(TemporalType.DATE)
	private Date dateOfOpening;
		
}
