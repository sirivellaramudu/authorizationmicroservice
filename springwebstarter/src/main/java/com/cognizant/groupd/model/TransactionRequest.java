package com.cognizant.groupd.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public  class  TransactionRequest {
    private int  accountNumber;
    private String transactionType;
    private double amount;
    
    
}


