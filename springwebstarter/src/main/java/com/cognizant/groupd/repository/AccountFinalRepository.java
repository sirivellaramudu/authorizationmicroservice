package com.cognizant.groupd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cognizant.groupd.entity.AccountFinal;
import com.cognizant.groupd.model.TransactionRequest;



@Repository
public interface AccountFinalRepository extends JpaRepository<AccountFinal, Integer> {
	
	@Query(value="Select a from AccountFinal a where a.custId=:custId")
	public List<AccountFinal> getAllAccount(@Param("custId") int custId);

}
