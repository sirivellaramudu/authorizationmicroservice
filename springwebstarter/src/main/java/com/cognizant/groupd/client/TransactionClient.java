package com.cognizant.groupd.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.cognizant.groupd.entity.AccountFinal;



@FeignClient(name="${transaction.name}", url="${transaction.url}")
public interface TransactionClient {
	@PutMapping(value = "/updateaccount/{accnumber}")
	public AccountFinal updateAccount(
			@RequestBody AccountFinal account, @RequestHeader(value = "Authorization", required = true) String requestTokenHeader);



}
