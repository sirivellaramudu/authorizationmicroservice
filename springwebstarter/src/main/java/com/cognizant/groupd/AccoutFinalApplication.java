package com.cognizant.groupd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AccoutFinalApplication {
    
	private static Logger logger = LoggerFactory.getLogger(AccoutFinalApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(AccoutFinalApplication.class, args);
		logger.info("Inside main");
	}

}
