package com.cognizant.groupd.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.groupd.entity.AccountFinal;
import com.cognizant.groupd.exception.AccountFinalNotFoundException;
import com.cognizant.groupd.exception.TokenExpireException;

import com.cognizant.groupd.model.TransactionRequest;
import com.cognizant.groupd.service.AccountFinalService;

@RestController
@CrossOrigin
public class AccountFinalController {

	private static Logger logger = LoggerFactory.getLogger(AccountFinalController.class);

	@Autowired
	AccountFinalService service;

	@PostMapping(value = "/registeraccount")
	public AccountFinal registerAccount(@RequestHeader("Authorization")String requestTokenHeader,@RequestBody AccountFinal account) throws TokenExpireException {
		logger.info("registerAccount() Started");
		account = service.registerAccount(account, requestTokenHeader);
		logger.info("AccountFinal = {}", account);
		logger.info("registerAccount() account end");
		return account;
	}

	@GetMapping(value = "/accountsbycustid/{custId}")
	public List<AccountFinal> getAllAccount(@PathVariable("custId") int custId,
			@RequestHeader("Authorization")String requestTokenHeader)
			throws TokenExpireException {
		logger.info("getAllAccount() Started");
		List<AccountFinal> accounts = service.getAllAccount(custId, requestTokenHeader);
		logger.info("AccountFinal = {}", accounts);
		logger.info("getAllAccount() end");

		return accounts;
	}

	@GetMapping(value = "/accountbyaccountid/{accNumber}")
	public AccountFinal getAccount(@PathVariable("accNumber") int accNumber,@RequestHeader("Authorization")String requestTokenHeader)
			throws AccountFinalNotFoundException, TokenExpireException {
		logger.info("getAccount() Started");
		AccountFinal account = service.getAccount(accNumber,requestTokenHeader);
		logger.info("AccountFinal = {}", account);
		logger.info("getAccount() end");
		return account;
	}

	@PutMapping(value = "/updateaccount")
	public AccountFinal updateAccount(@RequestBody TransactionRequest requestType,@RequestHeader("Authorization")String requestTokenHeader)
			throws AccountFinalNotFoundException, TokenExpireException {
		return service.updateAccount(requestType,requestTokenHeader);

	}

}
