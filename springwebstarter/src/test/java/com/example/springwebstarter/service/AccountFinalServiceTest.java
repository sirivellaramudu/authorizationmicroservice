package com.example.springwebstarter.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.cognizant.groupd.client.CustomerClient;
import com.cognizant.groupd.entity.AccountFinal;
import com.cognizant.groupd.exception.AccountFinalNotFoundException;
import com.cognizant.groupd.exception.TokenExpireException;

import com.cognizant.groupd.repository.AccountFinalRepository;
import com.cognizant.groupd.service.AccountFinalService;

public class AccountFinalServiceTest {
	
	@InjectMocks
	private AccountFinalService service;
	
	@Mock
	private AccountFinalRepository repo;
	
	@Mock
	private CustomerClient cust;
	
	  @Before
	  public void setUp() {
	    MockitoAnnotations.initMocks(this);
	  }

	@Test
	public void registerAccountTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(true);
		when(repo.save(Mockito.any())).thenReturn(acFinal);
		service.registerAccount(acFinal, "test");
	}	
	
	@Test(expected=Exception.class)
	public void registerAccountExceptionTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(false);
//		when(repo.save(Mockito.any())).thenThrow(new TokenExpireException("Token is Invalid"));
		service.registerAccount(acFinal, "test");
		
	}
	
	@Test
	public void getAllAccountTest() throws TokenExpireException {
		List<AccountFinal> mockList = new ArrayList<>();
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(true);
		when(repo.getAllAccount(51)).thenReturn(mockList);
		service.getAllAccount(1, "test");
	}
	
	@Test
	public void getAccountTest() throws AccountFinalNotFoundException, TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(12242);
		acFinal.setAccType("saving");
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(true);
		when(repo.findById(12242)).thenReturn(Optional.of(acFinal));
		service.getAccount(12242,"test");
	}
	
	@Test(expected=Exception.class)
	public void getAllAccountExceptionTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(false);
		service.getAllAccount(1, "test");
		
	}
	@Test(expected=Exception.class)
	public void getAccountException1Test() throws TokenExpireException, AccountFinalNotFoundException {
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(false);
		service.getAccount(1,"test");
		
	}
	
	@Test (expected=Exception.class)
	public void getAccountExceptionTest() throws AccountFinalNotFoundException, TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		when(cust.authorizeTheRequest(Mockito.anyString())).thenReturn(true);
		when(repo.findById(Mockito.any())).thenReturn(java.util.Optional.empty());
		
		service.getAccount(12242,"test");
		
	}

}
