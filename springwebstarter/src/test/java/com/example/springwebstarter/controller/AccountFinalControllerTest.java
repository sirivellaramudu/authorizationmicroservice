package com.example.springwebstarter.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.cognizant.groupd.controller.AccountFinalController;
import com.cognizant.groupd.entity.AccountFinal;
import com.cognizant.groupd.exception.AccountFinalNotFoundException;
import com.cognizant.groupd.exception.TokenExpireException;

import com.cognizant.groupd.service.AccountFinalService;

public class AccountFinalControllerTest {
	
	@InjectMocks
	private AccountFinalController controller;
	
	@Mock
	private AccountFinalService service;
	
		
	  @Before
	  public void setUp() {
	    MockitoAnnotations.initMocks(this);
	  }

	@Test
	public void registerAccountTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		when(service.registerAccount(Mockito.any(),Mockito.any())).thenReturn(acFinal);
		controller.registerAccount( "test",acFinal);
	}	
	
	@Test
	public void registerAccountExceptionTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		controller.registerAccount("test",acFinal);
		
	}
	
	@Test
	public void getAllAccountTest() throws TokenExpireException {
		List<AccountFinal> mockList = new ArrayList<>();
		controller.getAllAccount(1, "test");
	}
	
	@Test
	public void getAccountTest() throws AccountFinalNotFoundException, TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(12242);
		acFinal.setAccType("saving");
		controller.getAccount(12242,"test");
	}
	
	@Test 
	public void getAllAccountExceptionTest() throws TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		controller.getAllAccount(1, "test");
		
	}
	@Test
	public void getAccountException1Test() throws TokenExpireException, AccountFinalNotFoundException {
		controller.getAccount(1,"test");
		
	}
	
	@Test 
	public void getAccountExceptionTest() throws AccountFinalNotFoundException, TokenExpireException {
		AccountFinal acFinal = new AccountFinal();
		acFinal.setAccNo(1);
		acFinal.setAccType("saving");
		
		controller.getAccount(12242,"test");
		
	}

}
