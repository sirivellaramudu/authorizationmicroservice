package com.cognizant.groupd.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	private int custId;
	private String userName;
	private String password;
	
	public JwtRequest()
	{
		
	}

	public JwtRequest(int custId, String userName, String password) {
		this.setCustId(custId);
		this.setUserName(userName);
		this.setPassword(password);
		
	}
}