package com.cts.authorization.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.meanbean.test.BeanTester;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.groupd.AuthorizatiionMicroserviceApplication;
import com.cognizant.groupd.model.JwtRequest;

@SpringBootTest(classes=AuthorizatiionMicroserviceApplication.class)
class JwtRequestTest {

	private JwtRequest jwtReqAllArg = new JwtRequest(10023,"admin", "password");

	@Test
	void testUserNameGetter() {
		assertThat(jwtReqAllArg.getUserName().equals("admin")).isTrue();
	}
	
	@Test
    void testPensionerBean() {
        final BeanTester beanTester = new BeanTester();
        beanTester.getFactoryCollection();
        beanTester.testBean(JwtRequest.class);
    }


}
