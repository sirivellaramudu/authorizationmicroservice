package com.cts.authorization;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.groupd.AuthorizatiionMicroserviceApplication;

@SpringBootTest(classes=AuthorizatiionMicroserviceApplication.class)
class AuthorizatiionMicroserviceApplicationTests {

	@Test
	void main() {
		AuthorizatiionMicroserviceApplication.main(new String[] {});
	}
}
